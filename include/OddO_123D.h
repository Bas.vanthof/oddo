/** @brief C++ implementation of 3D 'from-to' arrays */
/**
*  In certain cases, it is very important that array's indices may start
*  at a chosen value, and not at 0. This is for example the case when
*  an array represents a part of a larger array.
*
*  The THIS_CLASS class allows a user to declare and address 3D 'from-to' arrays.
*/

#if CONST
#    define THIS_CLASS cat3(carray_,DIM,D)
#    define PREFIX C_
#    define CONSTORVAR const
#    define IFNOTCONST(a)
#else
#    define THIS_CLASS cat3(array_,DIM,D)
#    define PREFIX 
#    define CONSTORVAR 
#    define IFNOTCONST(a) a
#endif

template <class T, bool fortran_style>
class THIS_CLASS {

public:
#  if ! CONST
       void _destroy(void) {
           if (own_data) {
#              if ODDO_VERB_ALLOC
                   printf("%dd: deleting values %p for array %p\n",
                          DIM, (void*) values, (void*) this);
#              endif
               delete [] values;
           };
       };

       void create(const ptrdiff_t n1start_in, const ptrdiff_t n1end_in
#                  if DIM >= 2
                       , const ptrdiff_t n2start_in, const ptrdiff_t n2end_in
#                  endif
#                  if DIM >= 3
                       , const ptrdiff_t n3start_in, const ptrdiff_t n3end_in
#                  endif
                  ) {
           _destroy();
           n1start = n1start_in; n1end   = n1end_in;
#          if DIM >= 2
               n2start = n2start_in; n2end   = n2end_in;
#          endif
#          if DIM >= 3
               n3start = n3start_in; n3end   = n3end_in;
#          endif
      
           _allocate();
       };
      
       THIS_CLASS
          <T,  fortran_style> &copy(const
                                    THIS_CLASS
                                    <T,fortran_style> & other) {
           _destroy();
           n1start = other.n1start; n1end   = other.n1end;
#          if DIM >= 2
               n2start = other.n2start; n2end   = other.n2end;
#          endif
#          if DIM >= 3
               n3start = other.n3start; n3end   = other.n3end;
#          endif
           own_data = true;
      
           _allocate();
           memcpy(values, other.values, _nvalues() * sizeof(T));
           return *this;
       }
      
       void create(const size_t n1end_in
#          if DIM >= 2
               , const size_t n2end_in
#          endif
#          if DIM >= 3
               , const size_t n3end_in
#          endif
       ) {
           const ptrdiff_t ioff = fortran_style ? 1:0;
           create(ioff,n1end_in
#                 if DIM >= 2
                      ,ioff,n2end_in
#                 endif
#                 if DIM >= 3
                      ,ioff,n3end_in
#                 endif
                 );
       }
      
      
      
       void create(const ptrdiff_t nstart[DIM], const ptrdiff_t nend[DIM],
                   const T init) {
           create(nstart[0],nend[0]
#                 if DIM >= 2
                     ,nstart[1],nend[1]
#                 endif
#                 if DIM >= 3
                     ,nstart[2],nend[2]
#                 endif
                 );
           set_constant(init);
       }
      
#      if DIM > 1
           void create(const ptrdiff_t nstart[DIM], const ptrdiff_t nend[DIM]) {
               create(nstart[0],nend[0]
#                     if DIM >= 2
                          ,nstart[1],nend[1]
#                     endif
#                     if DIM >= 3
                          ,nstart[2],nend[2]
#                     endif
                     );
           }
#      endif
      
       ~THIS_CLASS(void) {
         _destroy();
       }
      
      
       THIS_CLASS
            (   const size_t n1end_in
#               if DIM >= 2
                    , const size_t n2end_in
#               endif
#               if DIM >= 3
                    , const size_t n3end_in
#               endif
       ) {
           own_data=false;
           create(n1end_in
#                 if DIM >= 2
                      , n2end_in
#                 endif
#                 if DIM >= 3
                      , n3end_in
#                 endif
                 );
       }
      
      
       THIS_CLASS
            (    const size_t n1end_in,
#                if DIM >= 2
                     const size_t n2end_in,
#                endif
#                if DIM >= 3
                     const size_t n3end_in,
#                endif
                 const T init) {
           own_data=false;
           create(n1end_in
#                 if DIM >= 2
                      , n2end_in
#                 endif
#                 if DIM >= 3
                      , n3end_in
#                 endif
                 );
           set_constant(init);
       }
      
       THIS_CLASS
            (   const ptrdiff_t nstart[DIM], const ptrdiff_t nend[DIM], const T init) {
           own_data=false;
           create(nstart,nend);
           set_constant(init);
       }
      
       THIS_CLASS
            (   const ptrdiff_t nstart[DIM], const ptrdiff_t nend[DIM]) {
           own_data=false;
           create(nstart,nend);
       }
      
      
       THIS_CLASS
            (   const ptrdiff_t n1start_in, const ptrdiff_t n1end_in,
#               if DIM >= 2
                    const ptrdiff_t n2start_in, const ptrdiff_t n2end_in,
#               endif
#               if DIM >= 3
                    const ptrdiff_t n3start_in, const ptrdiff_t n3end_in,
#               endif
                const T init) {
           own_data=false;
           create(n1start_in,n1end_in
#                 if DIM >= 2
                      , n2start_in,n2end_in
#                 endif
#                 if DIM >= 3
                      , n3start_in,n3end_in
#                 endif
                 );
           set_constant(init);
       }
      
       THIS_CLASS
            (   const ptrdiff_t n1start_in, const ptrdiff_t n1end_in
#               if DIM >= 2
                    , const ptrdiff_t n2start_in, const ptrdiff_t n2end_in
#               endif
#               if DIM >= 3
                    , const ptrdiff_t n3start_in, const ptrdiff_t n3end_in
#               endif
       ) {
           own_data=false;
           create(n1start_in,n1end_in
#                 if DIM >= 2
                      , n2start_in,n2end_in
#                 endif
#                 if DIM >= 3
                      , n3start_in,n3end_in
#                 endif
                 );
       }
      
      
      
       THIS_CLASS
            (   const
                THIS_CLASS
                & other) {
           own_data=false; copy(other);
       }
      
       //! Array element
       T& operator ()(const ptrdiff_t i1
#                     if DIM >= 2
                          , const ptrdiff_t i2
#                     endif
#                     if DIM >= 3
                          , const ptrdiff_t i3
#                     endif
       ) {
          return values[_get_idx(i1
#                                if DIM >= 2
                                     ,i2
#                                endif
#                                if DIM >= 3
                                     ,i3
#                                endif
                                )];
       }
      
       void set_constant( const T init )  {
           const size_t nvalues = _nvalues();
           for (size_t i=0; i<nvalues; i++) {
               values[i] = init;
           }
       };


       bool own_data; //!< true; array_2D object has done the allocation;
                      //!< false: array_2D object did no allocation
#  endif

   void create(const ptrdiff_t n1start_in, const ptrdiff_t n1end_in,
#              if DIM >= 2
                   const ptrdiff_t n2start_in, const ptrdiff_t n2end_in,
#              endif
#              if DIM >= 3
                   const ptrdiff_t n3start_in, const ptrdiff_t n3end_in,
#              endif
               CONSTORVAR T * const values_in) {
       IFNOTCONST(_destroy());
       n1start = n1start_in; n1end   = n1end_in;
#      if DIM >= 2
           n2start = n2start_in; n2end   = n2end_in;
#      endif
#      if DIM >= 3
           n3start = n3start_in; n3end   = n3end_in;
#      endif

       IFNOTCONST(own_data = false);
       values = values_in;
   };

#  if DIM > 1
       void create(const size_t n1end_in,
#                  if DIM >= 2
                       const size_t n2end_in,
#                  endif
#                  if DIM >= 3
                       const size_t n3end_in,
#                  endif
                   CONSTORVAR T * const values_in) {
           const ptrdiff_t ioff = fortran_style ? 1:0;
           create(ioff,n1end_in,
#                 if DIM >= 2
                      ioff,n2end_in,
#                 endif
#                 if DIM >= 3
                      ioff,n3end_in,
#                 endif
                  values_in);
       }
#endif
   void create(const ptrdiff_t nstart[DIM], const ptrdiff_t nend[DIM],
               CONSTORVAR T * const values_in) {
       create(nstart[0],nend[0],
#             if DIM >= 2
                  nstart[1],nend[1],
#             endif
#             if DIM >= 3
                  nstart[2],nend[2],
#             endif
              values_in);
   }

   THIS_CLASS(){
      IFNOTCONST(own_data=false);
      create(0,0
#            if DIM >= 2
                 , 0,0
#            endif
#            if DIM >= 3
                 , 0,0
#            endif
            );
   }

   THIS_CLASS
        (     const size_t n1end_in,
#             if DIM >= 2
                  const size_t n2end_in,
#             endif
#             if DIM >= 3
                  const size_t n3end_in,
#             endif
              CONSTORVAR T * const values_in) {
       IFNOTCONST(own_data=false);
       create(n1end_in,
#             if DIM >= 2
                  n2end_in,
#             endif
#             if DIM >= 3
                  n3end_in,
#             endif
              values_in);
   }

   THIS_CLASS
        (     const ptrdiff_t n1start_in, const ptrdiff_t n1end_in,
#             if DIM >= 2
                  const ptrdiff_t n2start_in, const ptrdiff_t n2end_in,
#             endif
#             if DIM >= 3
                  const ptrdiff_t n3start_in, const ptrdiff_t n3end_in,
#             endif
              CONSTORVAR T * const values_in) {
       IFNOTCONST(own_data=false);
       create(n1start_in,n1end_in,
#             if DIM >= 2
                  n2start_in,n2end_in,
#             endif
#             if DIM >= 3
                  n3start_in,n3end_in,
#             endif
              (CONSTORVAR T*) values_in);
   }

   THIS_CLASS
        (     const ptrdiff_t nstart[DIM], const ptrdiff_t nend[DIM],
              CONSTORVAR T * const values_in
#             if ! CONST
                  , const T init
#             endif
        ) {
       IFNOTCONST(own_data=false);
       create(nstart, nend,  values_in);
       IFNOTCONST(set_constant(init));
   }

    /** @name Adressing the array */
    ///@{
   //! Constant array element
   const T& operator ()(const ptrdiff_t i1
#                       if DIM >= 2
                            , const ptrdiff_t i2
#                       endif
#                       if DIM >= 3
                            , const ptrdiff_t i3
#                       endif
   ) const {
      return values[_get_idx(i1
#                            if DIM >= 2
                                 ,i2
#                            endif
#                            if DIM >= 3
                                 ,i3
#                            endif
                            )];
   }


   size_t get_size(const size_t i) const {
       if      (i==1) {return n1end-n1start;}
#      if DIM >= 2
           else if (i==2) {return n2end-n2start;}
#      endif
#      if DIM >= 3
           else if (i==3) {return n3end-n3start;}
#      endif
       else           {throw std::runtime_error( "only dimensions 1:3 exist");}
       return -12345;
   }

   CONSTORVAR T *values;  //!< Direct access to 'flat' array is allowed

   ptrdiff_t n1start, n1end;
#  if DIM >= 2
       ptrdiff_t n2start, n2end;
#  endif
#  if DIM >= 3
       ptrdiff_t n3start, n3end;
#  endif

private:
   size_t _get_idx(const ptrdiff_t i1
#                  if DIM >= 2
                       , const ptrdiff_t i2
#                  endif
#                  if DIM >= 3
                       , const ptrdiff_t i3
#                  endif
                   ) const {
       const ptrdiff_t ioff = fortran_style ? 1:0;
#        if ODDO_RANGECHECKING
              if (       n1start > i1 || i1>=n1end+ioff
#                 if DIM >= 2
                      || n2start > i2 || i2>=n2end+ioff
#                 endif
#                 if DIM >= 3
                      || n3start > i3 || i3>n3end+ioff
#                 endif
                 ) {
#                 if DIM == 1
                      printf("Illegal index (%d). Available: (%d:%d) %s-style\n",
#                 elif DIM == 2
                      printf("Illegal indices (%d,%d). Available: (%d:%d, %d:%d) %s-style\n",
#                 elif DIM == 3
                      printf("Illegal indices (%d,%d,%d). Available: (%d:%d, %d:%d, %d:%d) %s-style\n",
#                 endif
                     (int) i1,
#                    if DIM >= 2
                         (int) i2,
#                    endif
#                    if DIM >= 3
                         (int) i3,
#                    endif
                     (int) n1start, (int) n1end,
#                    if DIM >= 2
                         (int) n2start, (int) n2end,
#                    endif
#                    if DIM >= 3
                         (int) n3start, (int) n3end, 
#                    endif
                     fortran_style ? "FORTRAN" : "C"
                 );
              }
#        endif


#        if ODDO_RANGECHECKING
             if (n1start > i1 || i1>=n1end+ioff) {
                 printf("i1=%d, not in %d:%d\n",(int) i1, (int) n1start, (int) n1end);
                 if (n1start > i1) 
                       printf("    %d > %d\n", (int) n1start, (int) i1);
                 if (i1 >= n1end+ioff) 
                       printf("    %d >= %d\n", (int) i1, (int) (n1end + ioff));
                 throw std::runtime_error( "index 1 out of bounds");
             }
#            if DIM >= 2
                 if (n2start > i2 || i2>=n2end+ioff)
                     throw std::runtime_error( "index 2 out of bounds");
#            endif
#            if DIM >= 3
                 if (n3start > i3 || i3>=n3end+ioff)
                     throw std::runtime_error( "index 3 out of bounds");
#            endif
#        endif

      const long idx = 
           fortran_style ?  i1-n1start
#                           if DIM >= 2
                                + (n1end-n1start+1)*(i2-n2start)
#                           endif
#                           if DIM >= 3
                                + (n1end-n1start+1)*(n2end-n2start+1) * (i3-n3start)
#                           endif
                         :
#                           if DIM >= 3
                                (i3-n3start) +
                                (n3end-n3start)*
#                           endif
#                           if DIM >= 2
                                (i2-n2start) +
#                           endif
#                           if DIM >= 3
                                (n3end-n3start)*
#                           endif
#                           if DIM >= 2
                                (n2end-n2start)*
#                           endif
                            (i1-n1start);
      return idx;
   }

#  if !CONST
       size_t _empty() const {
          const ptrdiff_t ioff = fortran_style ? 1:0;
          return        n1end<=n1start-ioff
#                if DIM >= 2
                     || n2end<=n2start-ioff
#                endif
#                if DIM >= 3
                     || n3end<=n3start-ioff
#                endif
                 ;
       };
      
       size_t _nvalues() const {
           if (_empty()) return 0;
           const ptrdiff_t ioff = fortran_style ? 1:0;
           return _get_idx(n1end-1+ioff
#                          if DIM >= 2
                               , n2end-1+ioff
#                          endif
#                          if DIM >= 3
                               , n3end-1+ioff
#                          endif
                          ) + 1;
       };
      
       void _allocate(void) {
           const size_t nvalues = _nvalues();
           own_data = true;
#          if ODDO_VERB_ALLOC
               printf("%dd: allocating %d values array %p\n",
                      DIM, (size_t) nvalues, (void*) this);
#          endif
           values = new T[nvalues];
#          if ODDO_VERB_ALLOC
               printf("%dd: allocated values %p for array %p\n",
                      DIM, (void*) values, (void*) this);
#          endif
       };
      
#   endif
};

#if ! CONST
     template <class T_in, bool fortran_style_in, class T_out, bool fortran_style_out>
     void CopyArrays(const THIS_CLASS<T_in,fortran_style_in> &array_in,
                          THIS_CLASS<T_out,fortran_style_out> &array_out) {
         const size_t id_in  = (fortran_style_in?1:0),
                      id_out = (fortran_style_out?1:0);
         array_out.set_constant( (T_out) 0);
#        if DIM >= 3
             for (ptrdiff_t iz=std::max(array_out.n3start,      array_in.n3start);
                            iz<std::min(array_out.n3end+id_out ,array_in.n3end+id_in);
                            iz++) {
#        endif
#            if DIM >= 2
                 for (ptrdiff_t iy=std::max(array_out.n2start,      array_in.n2start);
                                iy<std::min(array_out.n2end+id_out, array_in.n2end+id_in);
                                iy++) {
#            endif
                 for (ptrdiff_t ix=std::max(array_out.n1start,      array_in.n1start);
                                ix<std::min(array_out.n1end+id_out, array_in.n1end+id_in);
                                ix++) {
                     array_out(ix
#                              if DIM >= 2
                                  , iy
#                              endif
#                              if DIM >= 3
                                  , iz
#                              endif
                              ) =
                          (T_out) array_in(ix
#                                          if DIM >= 2
                                              , iy
#                                          endif
#                                          if DIM >= 3
                                              , iz
#                                          endif
                                          );
                 }
#            if DIM >= 2
                 }
#            endif
#        if DIM >= 3
             }
#        endif
    }
#endif

#define TYPE(name) cat5(PREFIX,name,_,DIM,D)
#define FTYPE(name) cat2(f,TYPE(name))

typedef THIS_CLASS<bool,   ODDO_C_STYLE>     TYPE(bool);
typedef THIS_CLASS<int,    ODDO_C_STYLE>     TYPE(int);
typedef THIS_CLASS<double, ODDO_C_STYLE>     TYPE(Double);
typedef THIS_CLASS<size_t, ODDO_C_STYLE>     TYPE(Sizet);

typedef THIS_CLASS<double, ODDO_FORTRAN_STYLE>   FTYPE(Double);
typedef THIS_CLASS<int,    ODDO_FORTRAN_STYLE>   FTYPE(int);
typedef THIS_CLASS<bool,   ODDO_FORTRAN_STYLE>   FTYPE(bool);
#undef TYPE
#undef FTYPE
#undef PREFIX
#undef THIS_CLASS
#undef CONSTORVAR 
#undef IFNOTCONST

