#ifndef ODDO_H
#define ODDO_H

#include <stdexcept>
#include <cstdio>
#include <cstdlib>
#include <string.h>

// Settings (which you may want to change)
#define ODDO_RANGECHECKING 1
#define ODDO_VERB_ALLOC 0

// Definitions (not for changing)
#define ODDO_FORTRAN_STYLE (true)
#define ODDO_C_STYLE (false)

#define cat20(a,b)  a##b
#define cat2(a,b)   cat20(a,b)
#define cat3(a,b,c) cat2(cat2(a,b),c)
#define cat4(a,b,c,d) cat2(cat2(a,b),cat2(c,d))
#define cat5(a,b,c,d,e) cat2(cat2(a,b),cat2(cat2(c,d),e))
#define cat6(a,b,c,d,e,f) cat2(cat2(a,b),cat2(cat2(c,d),cat2(e,f)))
#define cat7(a,b,c,d,e,f,g) cat2(cat2(a,b),cat2(cat2(c,d),cat2(cat2(e,f),g)))

#define CONST 1
#    define DIM 1
#    include "OddO_123D.h"
#    undef DIM
#    define DIM 2
#    include "OddO_123D.h"
#    undef DIM
#    define DIM 3
#    include "OddO_123D.h"
#    undef DIM
#undef CONST 
#define CONST 0
#   define DIM 1
#   include "OddO_123D.h"
#   undef DIM
#   define DIM 2
#   include "OddO_123D.h"
#   undef DIM
#   define DIM 3
#   include "OddO_123D.h"
#   undef DIM
#undef CONST 
#endif
