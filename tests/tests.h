#ifndef ODDO_TESTS_H
#define ODDO_TESTS_H
#include "OddO.h"
void set_i_2d(Double_2D &a, const double scale);
void set_j_2d(Double_2D &a, const double scale);

void set_i_1d(Double_1D &a, const double scale);

void set_i_3d(Double_3D &a, const double scale);
void set_j_3d(Double_3D &a, const double scale);
void set_k_3d(Double_3D &a, const double scale);
#endif
