from oddo.Double_2D import Double_2D
from generated.Couple_testsC import set_i_2d, set_j_2d
from TsetSE.util import check_ref
import numpy as np
import os
import sys


def run():
    # Create a Double_2D without the array specified: the constructor will allocate an ndarray
    # Then, set the values in C++ and compare to the expected result.
    scale = 4.3
    a = Double_2D(-3, 3, 10, 12)
    set_i_2d(a,scale)
    refdata = { "a": np.array( [[ i * scale * (1+7e-9) for j in range(10,12) ] for i in range(-3,3) ] , dtype = np.double) }
    plotdata = { "a": a.values() }
    check_ref(plotdata, refdata, verbose=1)

    # Create a Double_2D with the array specified: the constructor will link it as a member variable
    # Then, set the values in C++ and compare to the expected result.
    scale = 5.2
    a = Double_2D(n1start = -2, n2start = 8, values = np.zeros( (5,4), dtype=np.double ))
    set_j_2d(a,scale)
    refdata = { "a": np.array( [[ j * scale * (1+8e-9) for j in range(8,8+4) ] for i in range(-2,-2+5) ] , dtype = np.double) }
    plotdata = { "a": a.values() }
    check_ref(plotdata, refdata, verbose=1)

