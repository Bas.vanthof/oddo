#!/usr/bin/env python3
from oddo.Double_2D import Double_2D
from generated.Couple_testsC import set_i_2d
import os
import sys


def run():
    scale = 4.3
    a = Double_2D(-3, 3, 10, 12)
    set_i_2d(a,scale)
    refdata = { "a": np.array( [[ i * scale for i in range(-3,3) ] for j in range(10,12) ]], dtype = np.double) }
    plotdata = { "a": a.values }
    print("diff = ",refdata['a'] - plotdata['a'])

