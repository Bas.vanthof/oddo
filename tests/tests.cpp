#include "OddO_2D.h"
#include "tests.h"
void set_i_2d(Double_2D &a, const double scale) {
    for (int i=a.n1start; i<a.n1end; i++) {
        for (int j=a.n2start; j<a.n2end; j++) {
            a(i,j) = i * scale;
        }
    }
}

void set_j_2d(Double_2D &a, const double scale) {
    for (int i=a.n1start; i<a.n1end; i++) {
        for (int j=a.n2start; j<a.n2end; j++) {
            a(i,j) = j * scale;
        }
    }
}

void set_i_1d(Double_1D &a, const double scale) {
    for (int i=a.n1start; i<a.n1end; i++) {
        a(i) = i * scale;
    }
}


void set_i_3d(Double_3D &a, const double scale) {
    for (int i=a.n1start; i<a.n1end; i++) {
        for (int j=a.n2start; j<a.n2end; j++) {
            for (int k=a.n3start; k<a.n3end; k++) {
                a(i,j,k) = i * scale;
            }
        }
    }
}

void set_j_3d(Double_3D &a, const double scale) {
    for (int i=a.n1start; i<a.n1end; i++) {
        for (int j=a.n2start; j<a.n2end; j++) {
            for (int k=a.n3start; k<a.n3end; k++) {
                a(i,j,k) = j * scale;
            }
        }
    }
}

void set_k_3d(Double_3D &a, const double scale) {
    for (int i=a.n1start; i<a.n1end; i++) {
        for (int j=a.n2start; j<a.n2end; j++) {
            for (int k=a.n3start; k<a.n3end; k++) {
                a(i,j,k) = k * scale;
            }
        }
    }
}


