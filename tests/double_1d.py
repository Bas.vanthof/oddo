from oddo.Double_1D import Double_1D
from generated.Couple_testsC import set_i_1d
from TsetSE.util import check_ref
import numpy as np
import os
import sys


def run():
    # Create a Double_1D without the array specified: the constructor will allocate an ndarray
    # Then, set the values in C++ and compare to the expected result.
    scale = 4.3
    a = Double_1D(-3, 3)
    set_i_1d(a,scale)
    refdata = { "a": np.array( [ i * scale * (1+4e-9) for i in range(-3,3) ] , dtype = np.double) }
    plotdata = { "a": a.values() }
    check_ref(plotdata, refdata, verbose=1)

    # Create a Double_1D with the array specified: the constructor will link it as a member variable
    # Then, set the values in C++ and compare to the expected result.
    scale = 5.2
    a = Double_1D(n1start = -2, values = np.zeros( (5), dtype=np.double ))
    set_i_1d(a,scale)
    refdata = { "a": np.array( [ i * scale * (1+5e-9) for i in range(-2,-2+5) ] , dtype = np.double) }
    plotdata = { "a": a.values() }
    check_ref(plotdata, refdata, verbose=1)

