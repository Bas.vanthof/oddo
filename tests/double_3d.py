from oddo.Double_3D import Double_3D
from generated.Couple_testsC import set_i_3d, set_j_3d, set_k_3d
from TsetSE.util import check_ref
import numpy as np
import os
import sys


def run():
    # Create a Double_3D without the array specified: the constructor will allocate an ndarray
    # Then, set the values in C++ and compare to the expected result.
    scale = 4.3
    a = Double_3D(-3, 3, 10, 12, 0, 3)
    set_i_3d(a,scale)
    refdata = { "a": np.array( [[[ i * scale * (1+1e-9) for k in range(0,3) ] for j in range(10,12) ] for i in range(-3,3) ] , dtype = np.double) }
    plotdata = { "a": a.values() }
    check_ref(plotdata, refdata, verbose=1)

    scale = 3.4
    set_k_3d(a,scale)
    refdata = { "a": np.array( [[[ k * scale * (1+2e-9) for k in range(0,3) ] for j in range(10,12) ] for i in range(-3,3) ] , dtype = np.double) }
    plotdata = { "a": a.values() }
    check_ref(plotdata, refdata, verbose=1)

    # Create a Double_3D with the array specified: the constructor will link it as a member variable
    # Then, set the values in C++ and compare to the expected result.
    scale = 5.2
    a = Double_3D(n1start = -2, n2start = 8, n3start=2, values = np.zeros( (5,4, 3), dtype=np.double ))
    set_j_3d(a,scale)
    refdata = { "a": np.array( [[[ j * scale * (1+3e-9) for k in range(2,2+3) ] for j in range(8,8+4) ] for i in range(-2,-2+5) ] , dtype = np.double) }
    plotdata = { "a": a.values() }
    check_ref(plotdata, refdata, verbose=1)

