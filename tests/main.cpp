#include "OddO_2D.h"
int main() {
    Double_2D a(-3, 3, 10, 12);
    for (int i=a.n1start; i<a.n1end; i++) {
        for (int j=a.n2start; j<a.n2end; j++) a(i,j) = i-j*0.2;
    }
    for (int i=-3; i<3; i++) {
        for (int j=10; j<12; j++) printf("a(%d,%d)=%e\n", i, j, a(i,j));
    }
    return 0;
}

