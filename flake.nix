{
  description = "OddO";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    TsetSE = {
      url = "git+https://gitlab.com/Bas.vanthof/tsetse.git";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
    C-ouPy = {
      url = "git+https://gitlab.com/Bas.vanthof/c-oupy.git";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
        TsetSE.follows = "TsetSE";
      };
    };
  };

  outputs = { self, nixpkgs, flake-utils, TsetSE, C-ouPy }:
    flake-utils.lib.eachDefaultSystem (system:
      let

        pkgs = import nixpkgs { inherit system; };

        nativeBuildInputs = [
          pkgs.cmake
          pkgs.python3Packages.scikit-build
          pkgs.texlive.combined.scheme-small
        ];

        propagatedBuildInputs = [
          TsetSE.packages.${system}.TsetSE
          C-ouPy.packages.${system}.C-ouPy
        ];

        checkInputs = [ 
           pkgs.python3Packages.pytest
           #pkgs.python3Packages.pytestCheckHook 
        ];

        OddO =
          pkgs.python3Packages.buildPythonPackage rec {
            pname = "oddo";
            version = "0.0.1";
            src = builtins.path { path = ./.; name = pname; };

            inherit nativeBuildInputs propagatedBuildInputs checkInputs;
            pytestFlagsArray = [ "--verbose" ];

            checkPhase = ''(cd tests && python -m pytest --verbose)'';

            pythonImportsCheck = [ "oddo" ];
            dontConfigure = true;
          };

      in {

        packages = {
          inherit OddO;
          default = OddO;
        };

        devShells = {
          default = pkgs.mkShell {
            name = "OddO-dev";
            venvDir = "./.venv";
            packages = [
              pkgs.python3Packages.venvShellHook
            ] ++ propagatedBuildInputs ++ [pkgs.python3Packages.pytest] ++ nativeBuildInputs;
          };
        };
      });
}
