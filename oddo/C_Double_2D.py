import numpy as np
from oddo.generated.C_Double_2DC import C_Double_2DC

class C_Double_2D(C_Double_2DC):
    def __init__(self, array, istart=[0, 0]):
        C_Double_2DC.__init__(self, istart[0], istart[0]+array.shape[0],
                              istart[1], istart[1]+array.shape[1], array)

