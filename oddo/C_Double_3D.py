import numpy as np
from oddo.generated.C_Double_3DC import C_Double_3DC


class C_Double_3D(C_Double_3DC):
    def __init__(self, array, istart=[0, 0, 0]):
        C_Double_3DC.__init__(self, istart[0], istart[0] + array.shape[0],
                              istart[1], istart[1]+array.shape[1],
                              istart[2], istart[2]+array.shape[2], array)

