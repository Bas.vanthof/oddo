from oddo.generated.Double_2DC import Double_2DC
from coupy.coupling import is_ndarray
import numpy as np

class Double_2D(Double_2DC):
    def __init__(self, n1start=0, n1end=None, n2start=0, n2end=None, values=None):
        if values is None:
            self._values = np.zeros( (n1end-n1start,n2end-n2start), dtype=np.double)
        else:
            assert is_ndarray(values, np.double), "values should be an ndarray of type double"
            assert len(values.shape)==2, "values should be None or a 2D array"
 
            n1end = n1start + values.shape[0] if n1end is None else n1end
            n2end = n2start + values.shape[1] if n2end is None else n2end

            assert n1end-n1start==values.shape[0], "dimension error in 1st dim"
            assert n2end-n2start==values.shape[1], "dimension error in 2nd dim"
            self._values = values

        Double_2DC.__init__(self, n1start, n1end, n2start, n2end, self._values)
        self._n1start = n1start
        self._n2start = n2start

    def values(self):
        return self._values

    def v(self,i,j):
        return self._values[i-self._n1start, j-self._n2start]

