from oddo.generated.Double_3DC import Double_3DC
from coupy.coupling import is_ndarray
import numpy as np

class Double_3D(Double_3DC):
    def __init__(self, n1start=0, n1end=None, n2start=0, n2end=None, n3start=0, n3end=None, values=None):
        if values is None:
            self._values = np.zeros( (n1end-n1start,n2end-n2start, n3end-n3start), dtype=np.double)
        else:
            assert is_ndarray(values, np.double), "values should be an ndarray of type double"
            assert len(values.shape)==3, "values should be None or a 3D array"
 
            n1end = n1start + values.shape[0] if n1end is None else n1end
            n2end = n2start + values.shape[1] if n2end is None else n2end
            n3end = n3start + values.shape[2] if n3end is None else n3end

            assert n1end-n1start==values.shape[0], "dimension error in 1st dim"
            assert n2end-n2start==values.shape[1], "dimension error in 2nd dim"
            assert n3end-n3start==values.shape[2], "dimension error in 3rd dim"
            self._values = values

        Double_3DC.__init__(self, n1start, n1end, n2start, n2end, n3start, n3end, self._values)
        self._n1start = n1start
        self._n2start = n2start
        self._n3start = n3start

    def values(self):
        return self._values

    def v(self,i,j,k):
        return self._values[i-self._n1start, j-self._n2start, k-self._n3start]

    def nstart(self):
        return self._n1start, self._n2start, self._n3start

    def range(self,idim):
        if idim==1:
            return range(self._n1start, self._n1start+self._values.shape[0])
        if idim==2:
            return range(self._n2start, self._n2start+self._values.shape[1])
        if idim==3:
            return range(self._n3start, self._n3start+self._values.shape[2])

    def size(self,idim):
        if idim==1:
            return self._n1start+self._values.shape[0]
        if idim==2:
            return self._n2start+self._values.shape[1]
        if idim==3:
            return self._n3start+self._values.shape[2]
           
