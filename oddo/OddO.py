#!/usr/bin/env python3
"""
Main script for OddO: from-to arrays in C++
This script is used to find the dll and the include directory
"""
import re
import os
import sys
import platform

def oddo_dir():
    return os.path.dirname(__file__)

def include_name():
    return os.path.join(oddo_dir(),'include')

def dll_name():
    dirname = os.path.join(oddo_dir(),'lib')
    filename = os.path.join(dirname,'libOddO.so')
    if platform.system() == 'Windows':
        filename = re.sub('.so$','.dll', filename)
    else:
        filename = re.sub('.dll$','.so', filename)
    return filename
    

if __name__ == "__main__":
    if '--include' in sys.argv:
        print(include_name())
    if '--dll' in sys.argv:
        print(dll_name())


