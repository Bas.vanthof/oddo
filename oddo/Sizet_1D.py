from oddo.generated.Sizet_1DC import Sizet_1DC
from ctypes import c_size_t
from coupy.coupling import is_ndarray
import numpy as np

class Sizet_1D(Sizet_1DC):
    def __init__(self, n1start=0, n1end=None, values=None):
        if values is None:
            self._values = np.zeros( (n1end-n1start), dtype=np.double)
        else:
            assert is_ndarray(values, c_size_t), "values should be an ndarray of type size_t"
            assert len(values.shape)==1, "values should be None or a 1D array"
 
            n1end = n1start + values.shape[0] if n1end is None else n1end

            assert n1end-n1start==values.shape[0], "dimension error"
            self._values = values

        Sizet_1DC.__init__(self, n1start, n1end, self._values)
        self._n1start = n1start

    def values(self):
        return self._values

    def v(self,i):
        return self._values[i-self._n1start]

