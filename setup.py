#!/usr/bin/env python3
from skbuild import setup  # This line replaces 'from setuptools import setup'
import setuptools

setup(
    name="OddO",
    version="0.0.1",
    author="Bas van 't Hof for Leiden University",
    author_email="vanthofbas@gmail.com",
    description="From-to arrays for C++, with some support in Python",
    url="https://gitlab.com/Bas.vanthof/oddo.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
      "coupy>=0.0.1"
    ],
    scripts = ['oddo/OddO.py'],
    include_package_data=True,
    package_data={
            "": ["*.json", "doc/*.pdf", "lib/*.so", 
                 "include/*.h", "generated/*.*"]}
)
