# OddO: from-to arrays in C++

# Installation

__OddO__ is installed automatically as a _submodule_ of __CapSys__. 
When installing __CapSys__, it is not necessary to also install __OddO__.

# Manual
The manual for __OddO__ is found in __pdfs/OddO_manual.pdf__
