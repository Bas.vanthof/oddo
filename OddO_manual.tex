\documentclass{article}
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}
\usepackage{float}
\usepackage{parskip}
\usepackage{graphicx}
\usepackage[margin=2cm]{geometry}
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[LE,LO]{
      \hspace{-0.1cm} \begin{tabular}{c} \\[-1ex] \includegraphics[width=30mm]{Logo/OddO-logo.pdf}
           \\{\Large OddO} \end{tabular}
}
\fancyfoot[LE,RO]{OddO manual, page \thepage}
\setlength{\headheight}{3cm}
\setlength{\textheight}{21cm}

\renewcommand{\familydefault}{\sfdefault}
\title{OddO, from-to arrays in C++ \\
       with some support in Python}
\author{Bas van 't Hof, for Leiden university}
\date{Dec 26, 2020}

\usepackage{xcolor}
\definecolor{darkgreen}{rgb}{0.0, 0.4, 0.26}
\definecolor{darkred}{rgb}{0.6, 0.0, 0.0}
\usepackage{listings}
\lstset{ 
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  commentstyle=\color{darkgreen},    % comment style
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
}

\begin{document}
\newcommand{\cmConst}[1]{{\color{darkred}#1}}
\newcommand{\cmCmd}[1]{{\color{blue}#1}}
\maketitle
\section{From-to arrays}
C++ offers a number of array-like objects. The simplest is offered by simply declaring a pointer, for instance
\begin{lstlisting}[language=C++,basicstyle=\ttfamily,columns=fullflexible]
double * array = new double[12];
\end{lstlisting}
With some effort, such pointers can also be used to represent 2D or 3D arrays.
For computational software, the problem with such arrays is that they {\em must} start at index zero, and that 
they do not 'know' their array size. It is therefore impossible to offer range checking in debug mode.

More advanced arrays, like the {\tt <deque>}s, {\tt <vector>}'s, {\tt <queue>}s, {\tt <array>}s etcetera that the 
Standard Template Library offers, focus on dynamic allocation (e.g. changing the object's dimension after creation),
do not have their data stored in a consecutive block of memory, which complicated data exchange with external 
systems (such as Python), and though efficient, may introduce unnecessary overhead in the inner loop of a 
numerical calculation.

OddO is a very small system that supports multi-dimensional arrays with chosen index ranges. It offers a number of
constructors, an overloaded assignment operator (which copies data), and simple indexing. Its internal state consists
of no more than its dimensions, a pointer to its data,
and a {\tt bool} indicating whether its data need to be deallocated when the destructor is called.

\section{Deallocation of the data}
Different OddO-arrays can be configured to refer to the same data. In such cases, at most one is the 
actual owner of the data, and will deallocate the data when it is destroyed. This 'original' array must 
therefore exist as long as any of the other arrays is still in use.

It is also possible to create an OddO-array that refers to externally allocated data. In such cases, the data 
have to also be deallocated externally when they are no longer needed.

\section{Simple example}
A simple program creates a 2D array and uses it:
\begin{lstlisting}[language=C++,basicstyle=\ttfamily,columns=fullflexible]
#include "OddO_2D.h"
int main() {
    Double_2D a(-3, 3, 10, 12);
    for (int i=a.n1start; i<a.n1end; i++) {
        for (int j=a.n2start; j<a.n2end; j++) a(i,j) = i-j*0.2;
    }
    for (int i=-3; i<3; i++) {
        for (int j=10; j<12; j++) printf("a(%d,%d)=%e\n", i, j, a(i,j));
    }
    return 0;
}
\end{lstlisting}
The program shows that the members of the array can be accessed, allowing the user to create shortcuts.

\section{Available {\tt typedef}s}
OddO uses {\tt <template>}s which allow you to use 1D, 2D or 3D arrays of any type, such as 
{\tt array\_1D<bool, ODDO\_C\_STYLE>  a(-1,10)}.
Special classes are available for arrays with constant data, such as {\tt carray\_2D<size\_t, ODDO\_C\_STYLE>  a(-3,10, 6,12)}.
Two {\em styles} of array-addressing are offered: 
\begin{itemize}
\item {\tt ODDO\_FORTRAN\_STYLE}, where
    \begin{itemize}
       \item the entry {\tt a(10)} exists in array {\tt a(-3, 10)};
       \item {\tt b(6,18)} and {\tt b(7,18)} are stored in consecutive memory locations in array {\tt b(4,7,  12,30)}.
    \end{itemize}
\item {\tt ODDO\_C\_STYLE}, where
    \begin{itemize}
       \item {\tt a(9)} is the last entry in array {\tt a(-3, 10)};
       \item {\tt b(6,18)} and {\tt b(6,19)} are stored in consecutive memory locations in array {\tt b(4,7,  12,30)}.
    \end{itemize}
\end{itemize}

A few specific array-types have been {\tt typedef}ed, for {\tt bool}s, {\tt int}s and {\tt double}s, such as {\tt C\_bool\_2D}.

\section{Python support}
A coupling to Python was made using {\tt C++ouPy}, for {\tt Double\_1D}, {\tt Double\_2D}, {\tt Double\_3D}.
The Python classes offer some flexibility in the python-constructor, similar to the C++ constructor.
There is also an indexing function, but it only works for reading the values, not for setting them.
Finally, the values can be obtained as an {\tt ndarray}, but when this is done, the indices all start at 0, and
offsets are to be used to get the correct addresssing.

\end{document}
